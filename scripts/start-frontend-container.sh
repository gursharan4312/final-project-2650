#!/bin/sh

docker run -d --rm --network my-net --name frontend \
    -v /home/ec2-user/project/frontend:/home/ec2-user/mnt/frontend \
    -w /home/ec2-user/mnt/frontend     bkoehler/feathersjs sh -c 'yarn install && yarn start'

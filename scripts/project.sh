#!/bin/bash
cd /home/ec2-user/project

sudo chmod -R 777 backend frontend

./scripts/start-backend-container.sh && ./scripts/start-frontend-container.sh

#!/bin/sh

docker run -d --rm --network my-net --name backend \
    -v /home/ec2-user/project/backend:/home/ec2-user/mnt/backend \
    -w /home/ec2-user/mnt/backend     bkoehler/feathersjs sh -c 'npm install && npm start'
